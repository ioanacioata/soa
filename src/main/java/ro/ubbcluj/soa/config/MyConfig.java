package ro.ubbcluj.soa.config;

import it.ozimov.springboot.mail.configuration.EnableEmailTools;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableEmailTools
public class MyConfig {

}
