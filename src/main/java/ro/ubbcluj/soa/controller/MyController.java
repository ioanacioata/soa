package ro.ubbcluj.soa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ro.ubbcluj.soa.service.EmailHandler;

@Controller
public class MyController {

  private final EmailHandler emailHandler;

  @Autowired
  public MyController(EmailHandler emailHandler) {
    this.emailHandler = emailHandler;
  }

  @GetMapping("/hello")
  public ResponseEntity<String> sayHello() {
    return new ResponseEntity<>("salut", HttpStatus.OK);
  }

  @GetMapping("/mail")
  public ResponseEntity<String> sendEmail(@RequestParam(value = "email") String email) {
    if ("".equals(email)) {
      return new ResponseEntity<>("Need the email address", HttpStatus.BAD_REQUEST);
    }
    emailHandler.sendEmail("test content", email);
    return new ResponseEntity<>("Email sent!", HttpStatus.OK);
  }
}
