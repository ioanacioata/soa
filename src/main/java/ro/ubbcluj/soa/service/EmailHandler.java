package ro.ubbcluj.soa.service;

import com.google.common.collect.Lists;
import it.ozimov.springboot.mail.model.Email;
import it.ozimov.springboot.mail.model.defaultimpl.DefaultEmail;
import it.ozimov.springboot.mail.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

@Component
public class EmailHandler {

  private final EmailService emailService;

  @Value("${spring.mail.username}")
  private String emailAddress;

  @Autowired
  public EmailHandler(EmailService emailService) {
    this.emailService = emailService;
  }

  public void sendEmail(String content, String email) {
    try {
      String subject = "Test subject";
      String body = "\n Hello!\n" + content;
      sendMailToAddress(subject, body, email);
    } catch (AddressException e) {
      System.out.println("Couldn'd send email");
    }
  }

  private void sendMailToAddress(String subject, String body, String email) throws AddressException {
    Email mail;
    mail = DefaultEmail.builder()
                       .from(new InternetAddress(emailAddress))
                       .to(Lists.newArrayList(new InternetAddress(email)))
                       .subject(subject)
                       .body(body)
                       .encoding("UTF-8")
                       .build();
    emailService.send(mail);
  }
}
