Application is on port **8085**
Project has two endpoints : 

- http://localhost:8085/hello 
- http://localhost:8085/mail?email=bianca_cioata@yahoo.com
	
Using Gmail as SMTP server from Java, Spring Boot app (https://www.javacodegeeks.com/2017/09/using-gmail-smtp-server-java-spring-boot-apps.html)

---

## Create the jar file

1. Use **mvn package** command
2. Find the jar file in **target/docker-spring-boot.jar**

---

## Docker commands for deploying application 

Followed the tutorial: https://www.youtube.com/watch?v=FlSup_eelYE 

1. **docker images**	 
	List all images that are available
  
2. **docker build -f Dockerfile -t docker-spring-boot .**	
	-f indicates where the Docker configurations are and -t is for giving a *tag* to the image
	Create the docker image.
  	
3. **docker run -p 8085:8085 docker-spring-boot**	
	run the docker image, map the ports	
	Create and run the docker container with the image created before
	
Now we can access the app from the localhost. It is deployed on docker.

4. **docker rmi -f 457**    
	Used to delete by force an image; instead of 457 put the first 3 characters from the image id

5. **docker container ls**  
	List all running containers

6. **docker container kill 814**  
	Stop a container, instead of 814 put the first 3 characters from the container id
